package com.pramati.test.basicPac
import math._

object FindingFixedPoints extends App{

  val tolerance=0.0001
  def isCloseEnough(x:Double,y:Double) = abs((x-y)/x)/x < tolerance
  def fixedPoint(f:Double=>Double)(firstGuess:Double) = {
    def iterate(guess:Double):Double = {
      val next=f(guess)
      if(isCloseEnough(guess, next)) next else iterate(next)
    }
    iterate(firstGuess)
  }
  
  println(fixedPoint(x=>1+x/2)(1))
}