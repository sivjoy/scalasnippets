package com.pramati.test.basicPac

object HigherOrderFunctions extends App{ 

	def sumBetweenIntegers(a:Integer,b:Integer) : Int = 
			if(a>b) 0 else a+sumBetweenIntegers(a+1, b)


			def cube(x:Int) :Int = x*x*x

			def sumOfCubesBetween(a:Int,b:Int) :Int = 
			if(a>b) 0 else cube(a)+sumOfCubesBetween(a+1,b)

			/*println(sumBetweenIntegers(1, 5))
println(sumOfCubesBetween(5,10))*/


			def sum(f: Int => Int,a: Int, b: Int): Int = {
					def loop(a: Int, acc: Int): Int = {
							if (a>b) acc else loop(a+1, f(a)+acc)
					}
					loop(a, 0)
			}

			println( sum(x=>x*x*x ,3,5))

}

