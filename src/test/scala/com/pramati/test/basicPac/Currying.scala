package com.pramati.test.basicPac

object Currying extends App{

  def mapReduce(f:Int=>Int,combine:(Int,Int)=>Int,zero:Int)(a:Int,b:Int):Int=
    if(a>b) zero  else combine(f(a),mapReduce(f,combine,zero)(a+1,b))
    
	def sum(f:Int=>Int) (a:Int,b:Int) :Int= 
		//if(a>b) 0 else f(a)+ sum(f)(a+1,b)
    mapReduce(f,(x,y)=>x+y,0)(a,b)
	
	def product(f:Int=>Int) (a:Int,b:Int):Int=
 // if(a>b) 1 else f(a)*product(f)(a+1,b)
   mapReduce(f,(x,y)=>x*y,1)(a,b)

  
  println(product(x=>x*x)(1,6))  
  println(sum(x=>x*x)(1,7)) 
    
}