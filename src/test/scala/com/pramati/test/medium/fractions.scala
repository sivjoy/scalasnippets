package com.pramati.test.medium;
/**
 * Created by sivaramt on 27-12-2014.
 */
object  fractions extends App{
  val r1 = new Rational(1,3)
  val r2 = new Rational(5,7)
  val r3= new Rational(3,5)
  val r4= new Rational(4)
  // printRational(r1.calc(x=>x*(-1),r2).calc(x=>x*(-1),r3))
  printRational(r2.calc(x=>(-x),r3))
  printRational(r2 + (x=>x ,r3))

  def printRational(r:Rational): Unit ={
    println("The value is : "+r.numer+"/"+r.denom)
  }
}

class Rational(x:Int,y:Int){
  //Require is used to check pre condition of the class
  //require(y!=0,"Invalid Denominator")
  //assert(y!=0,"Invalid Denominator")

  //Constructor def
  def this(x:Int)=this(x,1)

  private def gcd(a:Int,b:Int):Int= {if(b==0) a else gcd(b,a%b)}

  val numer=x/gcd(x,y)
  val denom=y/gcd(x,y)

  def +(f:Int=>Int ,that:Rational) = new Rational(numer*that.denom+that.numer*denom,denom*that.denom)
  def calc(f:Int=>Int ,that:Rational) = new Rational(numer*that.denom+f(that.numer)*denom,denom*that.denom)
}