object intSets {
  var t1=new NonEmpty(3,new Empty,new Empty)
  var t2 = t1 incl 4
}

abstract class IntSet{
  def contains(x:Int):Boolean
  def incl(x:Int):IntSet
}

object Empty extends IntSet{
  override def contains(x: Int): Boolean = false
  override def incl(x: Int): IntSet = new NonEmpty(x, Empty,Empty)
}

class NonEmpty(elem:Int,left:IntSet,right:IntSet) extends IntSet{
  override def contains(x: Int): Boolean =
  if  (x < elem) left contains x
  else if (x > elem) right contains x
  else true

  override def incl(x: Int): IntSet =
  if (x < elem) new NonEmpty(elem,left incl x,right )
  else if (x>elem) new NonEmpty(x,left,right incl elem)
  else this
}
