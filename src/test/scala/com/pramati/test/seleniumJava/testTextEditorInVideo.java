package com.pramati.test.seleniumJava;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class testTextEditorInVideo {
	WebDriver driver = null;
	WebDriverWait wait = null;

	@Test
	public void main() throws Exception{
		try	{
			driver = new FirefoxDriver();
			wait=new WebDriverWait(driver,30);
			driver.manage().window().maximize();
			driver.get("https://accounts.coursera.org/signin?mode=signin&post_redirect=%2F");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("signin-email")));
			driver.findElement(By.id("signin-email")).sendKeys("t.sivaram_6@yahoo.com");
			driver.findElement(By.id("signin-password")).sendKeys("Infosys_88");
			driver.findElement(By.xpath(".//button[text()='Sign In']")).click();
			Thread.sleep(4000);
			driver.get("https://class.coursera.org/progfun-005/lecture/75");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//iframe[@src='https://class.coursera.org/progfun-005/lecture/view?lecture_id=75']")));
			driver.switchTo().frame(driver.findElement(By.xpath(".//iframe[@src='https://class.coursera.org/progfun-005/lecture/view?lecture_id=75']")));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("QL_player_container_first")));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='mejs-time-total']")));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='mejs-cuepoint']")));
			driver.findElement(By.xpath(".//div[@class='mejs-cuepoint']")).click();
			driver.switchTo().defaultContent();
	}catch(Exception e){
		e.printStackTrace();
		driver.close();
		throw new Exception();
	}
	}
}

						
