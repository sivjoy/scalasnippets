package com.pramati.test.scalaProblems.lists



object Lists extends App{

	var list= List(1,2,3,4,3)
  var str=", ";

	def findLastElement1(methodList : List[Int]):List[Int]= methodList.takeRight(1)
			findLastElement1(list).foreach { x => println(x) } 

	def findLastElement2(methodList : List[Int]):Int= methodList.reverse.head
			println(findLastElement2(list))

	def findLastElement3(methodList : List[Int]):Int= methodList.last
			println(findLastElement3(list))

	def listFilter(methodList : List[Int]):List[Int]= {
    println("The filtered values are : ")
    methodList.filter { x => if(x%2==0)true else false } }
	    listFilter(list).foreach { x => println(x +" ") }
      
 def max(methodList : List[Int]):Int= methodList.max
      println("The max value is :"+max (list)) 
 /**
  * Need more clarity on this method.
  */
 def maxBy(methodList : List[Int]):Int= methodList.maxBy { x => x }
      println("The max By value is :"+maxBy (list))
      
 println("The mkrString function check : "+list.mkString(str))   
 
 println("The mkrString with start and end function check : "+list.mkString("{", ", ", "}"))
 
 println("The parallel implementation is : "+list.par)
 
 println("List partitioned for even and odd "+list.partition { x => (x%2==0) })
 
 println(list.productElement(2))
 

 
}